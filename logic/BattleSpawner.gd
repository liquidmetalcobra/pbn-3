extends Node

onready var field = $Battlefield
func _ready():

	var scene = load("res://Scenes/Megaman.tscn")	
	var p1 = scene.instance()
	add_child(p1)
	var fieldpath = field.get_path()

	
	p1.get_node("Player").setup(fieldpath,Vector2(1,1),0,$p1hp)
	field.setup(p1.get_node("Player").get_path(),null)


	var p2 = scene.instance()
	add_child(p2)
	p2.get_node("Player").setup(fieldpath,Vector2(4,1),1,$p2hp)
