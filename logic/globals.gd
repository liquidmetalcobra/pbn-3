extends Node

var player2id = -1
var Lobby = preload("res://Scenes/Lobby.tscn")
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

enum UIMoveLabelOrder {UIKEYRIGHT,UIKEYLEFT,UIKEYDOWN,UIKEYUP,UIATK1,UIATK2,UIATK3}
var UIMoveKeyBinds = [['ui_right','ui_left','ui_down','ui_up','attack1','attack2','attack3'],['ui_p2_right','ui_p2_left','ui_p2_down','ui_p2_up','attack1p2','attack2p2','attack3p2']]


enum AtkNames { ATTACKCANNON, ATTACKSWORD, ATTACKBOMB }
var AttackIDs = [0,7,5]
var AttackCoolDowns = [5,2,3]


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
