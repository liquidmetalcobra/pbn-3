extends KinematicBody2D
#export (NodePath) var path_to_battlefield = null
onready var animationPlayer = $AnimationPlayer
onready var moveSprite = $MoveSprite
onready var cannonSprite = $cannon
onready var swordSprite = $sword

#onready var 
var playerID = 0
var battleField
var healthBar
var setupDone = false
var spriteStateArray
var animationStateArray
var UICDArray = []
var health = 1000
enum { 	ENTERTILE,
		EXITTILE,
		IDLE,
		ATTACK1,
		ATTACK2,
		ATTACK3,
		ATTACK4,
		}
enum {	sMove,
		sAtk1,
		sAtk2,
		sAtk3,
		sAtk4, }

var SPEED=2
var cannon = preload("res://Scenes/CannonScene.tscn")
var sword = preload("res://Scenes/SwordScene.tscn")

var AtkCooldownUI = preload("res://Scenes/CooldownUIElement.tscn")

var state = IDLE
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var TILE_SIZE = Vector2(80,62.5);
var loc = Vector2(1,1)
var moveVector = Vector2(0,0)
var newLoc = Vector2(0,0)
func get_input():
	
	var moved = false
	match state:
		IDLE:
			
			moveVector = Vector2(0,0)
			
			if (Input.is_action_pressed(Globals.UIMoveKeyBinds[playerID][Globals.UIMoveLabelOrder.UIKEYRIGHT])):
				moved = true;
				if (loc.x <5):
					moveVector.x = 1
			if (Input.is_action_pressed(Globals.UIMoveKeyBinds[playerID][Globals.UIMoveLabelOrder.UIKEYLEFT])):
				moved = true;
				if (loc.x >0):
					moveVector.x = -1
			if (Input.is_action_pressed(Globals.UIMoveKeyBinds[playerID][Globals.UIMoveLabelOrder.UIKEYDOWN])):
				moved = true;
				if (loc.y <2):
					moveVector.y = 1
			if (Input.is_action_pressed(Globals.UIMoveKeyBinds[playerID][Globals.UIMoveLabelOrder.UIKEYUP])):
				moved = true;
				if (loc.y >0):
					moveVector.y = - 1
			if (Input.is_action_pressed(Globals.UIMoveKeyBinds[playerID][Globals.UIMoveLabelOrder.UIATK1]) and (UICDArray[0].isReady())):
				state = ATTACK1
				print ("setting state to atk1")
			if (Input.is_action_pressed(Globals.UIMoveKeyBinds[playerID][Globals.UIMoveLabelOrder.UIATK2]) and (UICDArray[1].isReady())):
				state = ATTACK2
			
				
					
			newLoc = loc + moveVector
			
			if (!battleField.requestLoc(newLoc,playerID)):
				newLoc = loc

			if (moved == true && newLoc != loc):
				state = EXITTILE
			
			updateState()


		

func updateState():
	match state:
		EXITTILE:
			animationPlayer.playback_speed = SPEED
			animationPlayer.play("exitTile");
		ATTACK1:	
			switchSprite(sAtk1)
			animationPlayer.play(animationStateArray[sAtk1])
			print ("starting timer")
			UICDArray[0].startTimer()
			
				
		ATTACK2:
			switchSprite(sAtk2)
			animationPlayer.play(animationStateArray[sAtk2])
			UICDArray[1].startTimer()
		
	#print (moved)
	#print (loc)
func sword_complete():
	print("sword done")
	switchSprite(sMove)
	state = IDLE
	
func attack_complete():
	var cannon_instance = cannon.instance()
	add_child(cannon_instance)
	cannon_instance.initialLocation = loc
	cannon_instance.playerID = playerID
	if (playerID == 0):
		cannon_instance.direction = "right"
	else:
		cannon_instance.direction = "left"
	cannon_instance.battleField = battleField
	cannon_instance.fire()
	switchSprite(sMove)
	state = IDLE

func reposition():
	loc = loc + moveVector
	position = loc*TILE_SIZE
	moveVector = Vector2(0,0)
	animationPlayer.play("enterTile");
	#yield(animationPlayer,"animation_finished")
	battleField.enterLocation(loc,get_node(self.get_path()))
	state = ENTERTILE
	
func move_complete():
	state = IDLE

func interact(obj):
	print ("got hit by nothing")
	health -= obj.damage
	healthBar.text = str(health)
	if (health <= 0):
		print ("BYE BYE")
		battleField.removeLocation(self)
		queue_free()
	
# Called when the node enters the scene tree for the first time.
func _ready():

	pass # Replace with function body.

func setup(path_to_field,initLocation,playerNumber=0,hpLabel=null):
	loc = initLocation
	playerID = playerNumber
	
	spriteStateArray = [moveSprite,cannonSprite,swordSprite,cannonSprite,cannonSprite]
	animationStateArray = ["move","Cannon","sword","Cannon","Cannon"]
	if (playerID == 1):
		for i in range(3):
			spriteStateArray[i].flip_h = true

	switchSprite(sMove)
	battleField = get_node(NodePath(path_to_field))
	
	battleField.enterLocation(loc,get_node(self.get_path()))
	position = loc*TILE_SIZE
	
	if (hpLabel):
		healthBar = hpLabel
		healthBar.text = str(health)
	UICDArray = [1,2,3,4]
	var atkNames = [Globals.AtkNames.ATTACKCANNON,Globals.AtkNames.ATTACKSWORD,Globals.AtkNames.ATTACKCANNON,Globals.AtkNames.ATTACKCANNON]
	for i in range(4):
		var atkCDUI = AtkCooldownUI.instance()
		get_parent().add_child(atkCDUI)
		atkCDUI.position = to_local(Vector2(200+450*playerID+60*i,100))
		atkCDUI.setup(atkNames[i])
		UICDArray[i] = atkCDUI
	
	
	setupDone = true
		
	
func switchSprite(spriteName):
	#print (sMove
	for i in range(3):
		if (spriteName == i):
			spriteStateArray[i].show()
		else:
			spriteStateArray[i].hide()
	
	
func _process(_delta):
	if (setupDone):
		get_input()
	#print(global_position)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
