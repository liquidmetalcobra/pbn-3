extends "res://logic/player.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func get_input():
	var moved = false
	var tempMoveVector = Vector2(0,0)
	
	if state == IDLE: 
		if (playerID == 0):
			if Input.is_action_pressed('ui_right'):
				moved = true;
				if (loc.x <5):
					tempMoveVector.x = 1
			if Input.is_action_pressed('ui_left'):
				moved = true;
				if (loc.x >0):
					tempMoveVector.x = -1
			if Input.is_action_pressed('ui_down'):
				moved = true;
				if (loc.y <2):
					tempMoveVector.y = 1
			if Input.is_action_pressed('ui_up'):
				moved = true;
				if (loc.y >0):
					tempMoveVector.y = - 1
			if Input.is_action_pressed('attack1'):
				state = ATTACK1
			if Input.is_action_pressed('attack2'):
				state = ATTACK2
		
		newLoc = loc + tempMoveVector
		
		if (!battleField.requestLoc(newLoc,playerID)):
			newLoc = loc

		if (moved == true && newLoc != loc):
			state = EXITTILE
		
		if is_network_master():
			moveVector = tempMoveVector
			updateState()	
		else:
			state = IDLE
		rpc_unreliable("_updateChar",tempMoveVector,state)
				

			


func updateState():
	match state:
		EXITTILE:
			animationPlayer.playback_speed = SPEED
			animationPlayer.play("exitTile");
		ATTACK1:
			switchSprite(sAtk1)
			animationPlayer.play("Cannon")
		ATTACK2:
			switchSprite(sAtk2)
			animationPlayer.play("sword")
remote func _updateChar(newMoveVector,newState):
	state = newState
	moveVector = newMoveVector
	if (state != IDLE):
		updateState()
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
