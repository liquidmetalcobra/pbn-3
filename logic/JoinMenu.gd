extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var ip = "127.0.0.1"
var port = 6969
onready var iptext = $IPv4Text
onready var porttext = $PortText
# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().connect("network_peer_connected",self,"_player_connected")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass



func _on_JoinButton_pressed():
	var net = NetworkedMultiplayerENet.new()
	net.create_client(ip,port)
	get_tree().set_network_peer(net)
	print ("joining")

	

func _on_BackButton_pressed():
	get_tree().change_scene_to(Globals.Lobby)


func _on_PortText_text_changed():
	port = int(porttext.text)


func _on_IPv4Text_text_changed():
	ip = iptext.text


func _player_connected(id):
	Globals.player2id = id
	var game = preload("res://Scenes/OnlineGame.tscn").instance()
	get_tree().get_root().add_child(game)
	hide()
