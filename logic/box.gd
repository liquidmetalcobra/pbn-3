extends Area2D
export (NodePath) var path_to_battlefield = null

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var health = 1000
var playerID = 1
var loc = Vector2(3,1)
var battleField
# Called when the node enters the scene tree for the first time.
func _ready():
	battleField = get_node(path_to_battlefield)
	battleField.enterLocation(loc,get_node(self.get_path()))
	pass # Replace with function body.


func interact(obj):
	health -= obj.damage
	print ("New HP: ",health)
	if (health <= 0):
		print ("BYE BYE")
		battleField.removeLocation(self)
		queue_free()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
