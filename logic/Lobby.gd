extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var single_player_level = preload("res://Scenes/Game.tscn")
var JoinMenu = preload("res://Scenes/JoinMenu.tscn")
var HostMenu = preload("res://Scenes/HostMenu.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():

	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_StartGame_pressed():
	get_tree().change_scene_to(single_player_level)


func _on_Host_pressed():
	get_tree().change_scene_to(HostMenu)
	
func _on_Join_pressed():
	get_tree().change_scene_to(JoinMenu)
	
