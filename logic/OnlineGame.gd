extends Node2D


onready var field = $Battlefield
func _ready():

	var scene = load("res://Scenes/OnlineMegaman.tscn")	
	var p1 = scene.instance()
	
	p1.set_name(str(get_tree().get_network_unique_id()))
	p1.set_network_master(get_tree().get_network_unique_id())
	
	add_child(p1)
	var fieldpath = field.get_path()

	
	p1.get_node("Player").setup(fieldpath,Vector2(1,1),0,$p1hp)
	field.setup(p1.get_node("Player").get_path(),null)


	var p2 = scene.instance()
	p2.set_name(str(str(Globals.player2id)))
	p2.set_network_master(Globals.player2id)
	
	
	add_child(p2)
	p2.get_node("Player").setup(fieldpath,Vector2(4,1),1,$p2hp)
	
	
