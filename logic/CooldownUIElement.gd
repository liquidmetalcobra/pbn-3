extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var cooldown
var cooldownTimestamp
var cooldownStartTime
var ready = true
onready var iconSprite = $cardIcon
onready var progressBar = $CircleNode/TextureProgress
onready var timer = $TimerNode/timer
# Called when the node enters the scene tree for the first time.
func _ready():
	print (self.global_position)
	print ("HI")
	cooldownTimestamp = OS.get_ticks_msec()
	cooldownStartTime = OS.get_ticks_msec()

	pass # Replace with function body.

func setup(id):
	cooldown = Globals.AttackCoolDowns[id]
	$cardicon.set_frame(Globals.AttackIDs[id])	
	print (self.global_position)


func isReady():
	return ready
	
	
func startTimer():
	cooldownTimestamp = cooldown*1000 + OS.get_ticks_msec()
	cooldownStartTime = OS.get_ticks_msec()
	ready = false
	print(cooldownTimestamp)
	print (cooldownStartTime)
	
func _process(_delta):
	
	if (ready ==false):
	
		var msElapsed = OS.get_ticks_msec() - cooldownStartTime
		timer.text = String((cooldown*1000-msElapsed)/1000)
		progressBar.value = 100 * float(msElapsed)/float(cooldown*1000)
		print (progressBar.value)
	if (OS.get_ticks_msec()> cooldownTimestamp ):
		ready = true
		timer.text = ""
		progressBar.value = 0	
	
	
	
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
