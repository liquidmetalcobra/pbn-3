extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var player1 = $Player
onready var background = $Background
onready var foreground = $Foreground
export (NodePath) var path_to_player = null
export var number = 5
var p1
var SETBACKGROUND=true
var SETFOREGROUND=false
var ownership = [[0,0,0],[0,0,0],[0,0,0],[1,1,1],[1,1,1],[1,1,1]]
var entities = {}
enum {
	EMPTY = -1,
	P1NORMAL,
	P1CRACKED,
	P1EMPTY,
	P1ICY,
	P1GRASSY,
	CHARPRESENT,
	TARGETED,
	P2NORMAL
}
# Called when the node enters the scene tree for the first time.
func _ready():
	#p1 = get_node(path_to_player)
	#print (p1.get("loc"))
	# Now you can access its variables like normal.
	

	pass # Replace with function body.


func setup(p1_path,p2_path):
	if (p1_path):
		p1 = get_node(p1_path)
		print (p1.get("loc"))
func requestLoc(loc,playerID):
	if (ownership[loc.x][loc.y] == playerID):
		return true
	else:
		return false
	
func removeLocation (obj):
	entities.erase(obj)
	
func enterLocation(loc, obj): #use for if an object is physically present
	print ("Logical ", loc)
	
	var objFound = false
	for entity in entities.keys():
		#print entities[
		if (obj == entity):
			var tileLoc =  background.world_to_map(obj.global_position)
			print ("Global ", tileLoc)
			if (obj.playerID == 0):
				setTileCell(entities[obj],EMPTY,SETFOREGROUND)
				setTileCell(loc,CHARPRESENT,SETFOREGROUND)
			else:
				setTileCell(entities[obj],EMPTY,SETFOREGROUND)
				setTileCell(loc,CHARPRESENT,SETFOREGROUND)
			entities[obj] = loc
			objFound = true
		elif (loc == entities[entity]):
			obj.interact(entity)
			entity.interact(obj)#be careful not to let death break the game
			
			pass #collision
	if !objFound:
		entities[obj] = loc
	

func attackLocation (loc, obj): #use for if an object wants to see if that location is present, and if so, interact
	var returnValue = false
	for entity in entities.keys():
		if (obj != entity && loc == entities[entity]):
			obj.interact(entity)
			entity.interact(obj)#be careful not to let death break the game
			returnValue = true
	return returnValue
	
	
func setTileCell(loc,tileID, setBackground):
	#loc.x *= 2 
	
	loc.y -=3
	print ("Trying to set ",loc)
	if (setBackground):
		background.set_cell(loc.x,loc.y,tileID)
	else:
		foreground.set_cell(loc.x,loc.y,tileID)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
